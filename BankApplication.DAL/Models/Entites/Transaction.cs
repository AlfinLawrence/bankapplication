﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BankApplication.DAL.Models.Entites
{
    public class Transaction
    {
        [Key]
        public Guid TransactionId { get; set; }

        [ForeignKey("CustomerId")]
        public Guid CustomerId { get; set; }
        public Customer Customer { get; set; }

        public DateTime TransactionDate { get; set; }

        public decimal Deposit { get; set; }

        public decimal Withdraw { get; set; }

        public decimal Balance { get; set; }
    }
}
