﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BankApplication.DAL.Models.VirtualModels
{
    public class TransactionModel
    {
        [Required(ErrorMessage = "Enter valid data")]
        [Range(1,500000, ErrorMessage = "Enter valid Amount Range [1 - 500000]")]
        public decimal Amount { get; set; }
    }

}
