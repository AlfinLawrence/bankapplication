﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BankApplication.DAL.Models.VirtualModels
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Email is required")]
        [MaxLength(50)]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [MaxLength(250)]
        public string Password { get; set; }
    }
}
