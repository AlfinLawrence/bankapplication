﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BankApplication.DAL.Models.VirtualModels
{
    public class TransactionHistory
    {
        public DateTime TransactionDate { get; set; }

        public decimal Deposit { get; set; }

        public decimal Withdraw { get; set; }

        public decimal Balance { get; set; }
    }
}
