﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BankApplication.DAL.Models.VirtualModels
{
    public class TransactionResponseModel : ResponseModel
    {
        public decimal Balance { get; set; }

        public decimal Amount { get; set; }
    }
}
