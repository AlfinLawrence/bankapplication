﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BankApplication.DAL.Models.VirtualModels
{
    public class ResponseModel
    {
        public string StatusCode { get; set; }

        public string Message { get; set; }
    }
}
