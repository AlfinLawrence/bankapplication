﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BankApplication.DAL.Models.VirtualModels
{
    public class ProfileModel
    {

        public string Name { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }
        public DateTime DOB
        {
            get; set;
        }
    }
}
