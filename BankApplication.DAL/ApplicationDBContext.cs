﻿using BankApplication.DAL.Models.Entites;
using BankApplication.DAL.Models.Identity;
using Microsoft.EntityFrameworkCore;
using System;

namespace BankApplication.DAL
{
    public class ApplicationDBContext : DbContext
    {
        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options) : base(options)
        {
            Database.Migrate();
        }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Transaction> Transactions { get; set; }
    }
}
