﻿using BankApplication.DAL.Models.Entites;
using BankApplication.DAL.Models.VirtualModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankApplication.DAL.Repository
{
    public interface ITransactionRepository
    {
        decimal GetAccountBalance(Guid Customerid);

        bool Withdraw(Guid Customerid, decimal amount);

        bool Deposit(Guid Customerid, decimal amount);

        List<TransactionHistory> GetTransactions(Guid Customerid);

    }

    public class TransactionRepository : ITransactionRepository
    {
        private readonly ApplicationDBContext db;
        public TransactionRepository(ApplicationDBContext db)
        {
            this.db = db;
        }
        public decimal GetAccountBalance(Guid Customerid)
        {
            var lastTransaction = db.Transactions.Where(a => a.CustomerId == Customerid).OrderByDescending(a => a.TransactionDate).FirstOrDefault();

            if(lastTransaction != null)
            {
                return lastTransaction.Balance;
            }
            else
            {
                return 0;
            }
        }

        public bool Withdraw(Guid Customerid, decimal amount)
        {
            var balanceamt = GetAccountBalance(Customerid);
            Transaction withdraw = new Transaction();
            withdraw.TransactionId = new Guid();
            withdraw.CustomerId = Customerid;
            withdraw.Balance = balanceamt - amount;
            withdraw.Deposit = 0;
            withdraw.Withdraw = amount;
            withdraw.TransactionDate = System.DateTime.Now;
            db.Transactions.Add(withdraw);
            db.SaveChanges();
            return true;
        }

        public bool Deposit(Guid Customerid, decimal amount)
        {
            var balanceamt = GetAccountBalance(Customerid);
            Transaction deposit = new Transaction();
            deposit.TransactionId = new Guid();
            deposit.CustomerId = Customerid;
            deposit.Balance = balanceamt + amount;
            deposit.Deposit = amount;
            deposit.Withdraw = 0;
            deposit.TransactionDate = System.DateTime.Now;
            db.Transactions.Add(deposit);
            db.SaveChanges();
            return true;
        }

        public List<TransactionHistory> GetTransactions(Guid Customerid)
        {
            var transactionlist = db.Transactions.Where(a => a.CustomerId == Customerid).OrderBy(a=>a.TransactionDate).ToList();
            List <TransactionHistory> result = transactionlist.Select(a =>
                           new TransactionHistory
                           {
                               TransactionDate = a.TransactionDate,
                               Deposit = a.Deposit,
                               Withdraw = a.Withdraw,
                               Balance = a.Balance
                           }).ToList();
            return result;    
        }
    }
}
