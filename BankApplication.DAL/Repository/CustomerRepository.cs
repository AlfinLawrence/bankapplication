﻿using BankApplication.DAL.Models.Entites;
using BankApplication.DAL.Models.VirtualModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankApplication.DAL.Repository
{
    public interface ICustomerRepository
    {
        public Boolean IsCustomerExist(string Email);

        bool AddCustomer(Customer _Customer);

        Customer GetUserByUsernameAndPassword(LoginModel loginModel);

        Customer GetCustomerProfileById(Guid CustomerId);

        Boolean IsCustomerExistOnEdit(string Email, Guid CustomerId);

        bool UpdateCustomer(Customer _Customer);
    }

    public class CustomerRepository : ICustomerRepository
    {
        private readonly ApplicationDBContext db;
        public CustomerRepository(ApplicationDBContext db)
        {
            this.db = db;
        }

        public Boolean IsCustomerExist(string Email)
        {
            var _customer = db.Customers.Where(a => a.Email == Email).FirstOrDefault();

            if(_customer == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public Boolean IsCustomerExistOnEdit(string Email, Guid CustomerId)
        {
            var _customer = db.Customers.Where(a => a.Email == Email && a.CustomerId != CustomerId).FirstOrDefault();

            if (_customer == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool AddCustomer(Customer _Customer)
        {
            db.Customers.Add(_Customer);
            db.SaveChanges();
            return true;
        }

        public bool UpdateCustomer(Customer _Customer)
        {
            var MCustomer = db.Customers.Find(_Customer.CustomerId);
            MCustomer.DOB = _Customer.DOB;
            MCustomer.Email = _Customer.Email;
            MCustomer.Name = _Customer.Name;
            MCustomer.Password = _Customer.Password;

            db.Entry(MCustomer).State = EntityState.Modified;
            db.SaveChanges();
            return true;
        }

        public Customer GetUserByUsernameAndPassword(LoginModel loginModel)
        {
            var user = db.Customers.Where(a => a.Email == loginModel.Email && a.Password == loginModel.Password).FirstOrDefault();
            return user;
        }

        public Customer GetCustomerProfileById(Guid CustomerId)
        {
            var user = db.Customers.Find(CustomerId);
            return user;
        }
    }
}
