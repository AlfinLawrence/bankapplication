﻿using BankApplication.DAL.Models.Entites;
using BankApplication.DAL.Models.VirtualModels;
using BankApplication.DAL.Repository;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankApplication.Service
{
    public interface IProfileService
    {
        ProfileModel GetCustomerProfileById();

        ResponseModel EditUser(Customer _Customer);
    }

    public class ProfileService : IProfileService
    {
        private readonly Guid Customerid;

        private readonly IHttpContextAccessor httpContextAccessor;

        private readonly ICustomerRepository ICustomerRepository;
        public ProfileService(ICustomerRepository ICustomerRepository, IHttpContextAccessor httpContextAccessor)
        {
            this.ICustomerRepository = ICustomerRepository;
            this.httpContextAccessor = httpContextAccessor;
            this.Customerid = GetUserId();
        }

        public ProfileModel GetCustomerProfileById()
        {
            var customer = ICustomerRepository.GetCustomerProfileById(Customerid);
            return new ProfileModel
            {
                Name = customer.Name,
                Email = customer.Email,
                DOB = customer.DOB,
                Password = StringCipher.Decrypt(customer.Password)
            };
        }

        public ResponseModel EditUser(Customer _Customer)
        {
            _Customer.CustomerId = Customerid;
            if (ICustomerRepository.IsCustomerExistOnEdit(_Customer.Email, _Customer.CustomerId))
            {
                return new ResponseModel { StatusCode = "Error", Message = "Email already exist." };
            }
            else
            {
                _Customer.Password = StringCipher.Encrypt(_Customer.Password);
                if (ICustomerRepository.UpdateCustomer(_Customer))
                {
                    return new ResponseModel { StatusCode = "Success", Message = "Edit Profile Success." };
                }
                else
                {
                    return new ResponseModel { StatusCode = "Error", Message = "An Error Occured." };
                }
            }
        }

        private Guid GetUserId()
        {
            var customer = this.httpContextAccessor.HttpContext.
                         User.Claims.FirstOrDefault(x => x.Type.Equals("sid", StringComparison.InvariantCultureIgnoreCase)).Value;
            return Guid.Parse(customer.ToString());
        }
    }
}
