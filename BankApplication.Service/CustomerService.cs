﻿using BankApplication.DAL.Models.Entites;
using BankApplication.DAL.Models.VirtualModels;
using BankApplication.DAL.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;


namespace BankApplication.Service
{
    public interface ICustomerService
    {
        ResponseModel AddUser(Customer _Customer);

        ResponseModel LoginUser(LoginModel model);


    }

    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository ICustomerRepository;
        private IConfiguration _config;

        public CustomerService(ICustomerRepository ICustomerRepository, IConfiguration config)
        {
            this.ICustomerRepository = ICustomerRepository;
            this._config = config;
        }

        public ResponseModel AddUser(Customer _Customer)
        {
            if(ICustomerRepository.IsCustomerExist(_Customer.Email))
            {
                return new ResponseModel { StatusCode = "Error", Message = "Email already exist." };
            }
            else
            {
                _Customer.Password = StringCipher.Encrypt(_Customer.Password);
                if (ICustomerRepository.AddCustomer(_Customer))
                {
                    return new ResponseModel { StatusCode = "Success", Message = "Sign Up Success." };
                }
                else
                {
                    return new ResponseModel { StatusCode = "Error", Message = "An Error Occured." };
                }
            }
        }

        public ResponseModel LoginUser(LoginModel model)
        {
            model.Password = StringCipher.Encrypt(model.Password);
            var user = ICustomerRepository.GetUserByUsernameAndPassword(model);

            if(user != null)
            {
                var token = GenerateJSONWebToken(user);
                return new ResponseModel { StatusCode = "Success", Message = token };
            }
            else
            {
                return new ResponseModel { StatusCode = "Error", Message = "Invalid Login" };
            }
        }

        private string GenerateJSONWebToken(Customer model)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[] {
            new Claim(JwtRegisteredClaimNames.Sid, model.CustomerId.ToString()),
            new Claim(JwtRegisteredClaimNames.Sub, model.Name),
            new Claim(JwtRegisteredClaimNames.Email, model.Email),
            new Claim(ClaimTypes.Role, "Customer")
        };


            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
              claims,
              expires: DateTime.Now.AddMinutes(120),
              signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }



    }
}
