﻿using BankApplication.DAL.Models.VirtualModels;
using BankApplication.DAL.Repository;
using IdentityModel;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BankApplication.Service
{
    public interface ITransactionService
    {
        TransactionResponseModel Withdraw(decimal amount);

        TransactionResponseModel Deposit(decimal amount);

        List<TransactionHistory> GetTransactions();

        TransactionResponseModel GetBalance();
    }

    public class TransactionService : ITransactionService
    {
        private readonly ITransactionRepository _TransactionRepository;

        private readonly IHttpContextAccessor httpContextAccessor;

        private readonly Guid Customerid;

        public TransactionService(ITransactionRepository TransactionRepository, IHttpContextAccessor httpContextAccessor)
        {
            this._TransactionRepository = TransactionRepository;
            this.httpContextAccessor = httpContextAccessor;
            this.Customerid = GetUserId();
        }

        public TransactionResponseModel Withdraw(decimal amount)
        {
            if(amount > _TransactionRepository.GetAccountBalance(Customerid))
            {
                return new TransactionResponseModel { StatusCode = "Error", Message = "Your account balance is low than requested amount.", Amount = amount, Balance = _TransactionRepository.GetAccountBalance(Customerid) };
            }
            else
            {
                if(_TransactionRepository.Withdraw(Customerid,amount))
                {
                    return new TransactionResponseModel { StatusCode = "Success", Message = "Withdraw successful.", Amount = amount, Balance = _TransactionRepository.GetAccountBalance(Customerid) };
                }
                else
                {
                    return new TransactionResponseModel { StatusCode = "Error", Message = "An Error Occured.", Amount = amount, Balance = _TransactionRepository.GetAccountBalance(Customerid) };
                }
            }
        }

        public TransactionResponseModel Deposit(decimal amount)
        {
            if (_TransactionRepository.Deposit(Customerid, amount))
            {
                return new TransactionResponseModel { StatusCode = "Success", Message = "Desposit successful.", Amount = amount, Balance = _TransactionRepository.GetAccountBalance(Customerid) };
            }
            else
            {
                return new TransactionResponseModel { StatusCode = "Error", Message = "An Error Occured." };
            }
        }

        public List<TransactionHistory> GetTransactions()
        {
            return _TransactionRepository.GetTransactions(Customerid);
        }

        public TransactionResponseModel GetBalance()
        {
            var balamt = _TransactionRepository.GetAccountBalance(Customerid);
            return new TransactionResponseModel { StatusCode = "Success", Message = "Your Account Balance is " + balamt.ToString(), Balance = _TransactionRepository.GetAccountBalance(Customerid) };
        }

        private Guid GetUserId()
        {
            var customer = this.httpContextAccessor.HttpContext.
                         User.Claims.FirstOrDefault(x => x.Type.Equals("sid", StringComparison.InvariantCultureIgnoreCase)).Value;
            return Guid.Parse(customer.ToString());
        }
    }
}
