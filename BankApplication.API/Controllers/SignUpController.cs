﻿using BankApplication.DAL.Models.Entites;
using BankApplication.DAL.Models.VirtualModels;
using BankApplication.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankApplication.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SignUpController : ControllerBase
    {
        private readonly ICustomerService ICustomerService;
        public SignUpController(ICustomerService ICustomerService)
        {
            this.ICustomerService = ICustomerService;
        }

        [HttpPost]
        public ResponseModel Post(Customer _Customer)
        {
                _Customer.CustomerId = new Guid();
                return ICustomerService.AddUser(_Customer);
        }
    }
}
