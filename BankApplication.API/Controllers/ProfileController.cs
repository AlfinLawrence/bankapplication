﻿using BankApplication.DAL.Models.Entites;
using BankApplication.DAL.Models.VirtualModels;
using BankApplication.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankApplication.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ProfileController : ControllerBase
    {
        private readonly IProfileService _IProfileService;
        public ProfileController(IProfileService _IProfileService)
        {
            this._IProfileService = _IProfileService;
        }

        [HttpGet]
        public ProfileModel GetProfile()
        {
           
            return _IProfileService.GetCustomerProfileById();
        }

        [HttpPost]
        public ResponseModel EditProfile(Customer _customer)
        {
            return _IProfileService.EditUser(_customer);
        }
    }
}
