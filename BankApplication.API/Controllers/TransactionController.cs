﻿using BankApplication.DAL.Models.VirtualModels;
using BankApplication.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankApplication.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize(Roles = "Customer")]
    public class TransactionController : ControllerBase
    {
        private readonly ITransactionService _ITransactionService;

        public TransactionController(ITransactionService _ITransactionService)
        {
            this._ITransactionService = _ITransactionService;
        }

        [HttpGet]
        public TransactionResponseModel Balance()
        {
            //var CustomerId = GetUserid();
            return _ITransactionService.GetBalance();
        }

        [HttpPost]
        public TransactionResponseModel Deposit(TransactionModel model)
        {
            //var CustomerId = GetUserid();
            return _ITransactionService.Deposit(model.Amount);
        }

        [HttpPost]
        public TransactionResponseModel WithDraw(TransactionModel model)
        {
            //var CustomerId = GetUserid();
            return _ITransactionService.Withdraw(model.Amount);
        }

        public List<TransactionHistory> GetTransactions()
        {
            //var CustomerId = GetUserid();
            return _ITransactionService.GetTransactions();
        }

        //private Guid GetUserid()
        //{
        //    var customer = User.Claims.FirstOrDefault(x => x.Type.Equals("sid", StringComparison.InvariantCultureIgnoreCase)).Value;
        //    var CustomerId = Guid.Parse(customer.ToString());
        //    return CustomerId;
        //}
    }
}
