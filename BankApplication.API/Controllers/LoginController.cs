﻿using BankApplication.DAL.Models.Entites;
using BankApplication.DAL.Models.VirtualModels;
using BankApplication.Service;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankApplication.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly ICustomerService ICustomerService;

        public LoginController(ICustomerService ICustomerService)
        {
            this.ICustomerService = ICustomerService;
        }


        [HttpPost]
        public ResponseModel Post(LoginModel model)
        {
            return ICustomerService.LoginUser(model);
        }
    }
}
