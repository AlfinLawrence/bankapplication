﻿using BankApplication.DAL.Models.VirtualModels;
using BankApplication.Service;
using FakeItEasy;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace BankApplication.Tests.ServiceTests
{
    public class TransactionServiceTest
    {
        [Fact]
        public void Deposit_ReturnsResponseModel_WhenDepositAmount()
        {

            var repo = A.Fake<ITransactionService>();
            var result = A.Fake<TransactionResponseModel>();
            var transactionobj = A.Fake<TransactionModel>();

            // set up a call to return a value
            A.CallTo(() => repo.Deposit(transactionobj.Amount)).Returns(result);
        }

        [Fact]
        public void WithDraw_ReturnsResponseModel_WhenWithdrawAmount()
        {

            var repo = A.Fake<ITransactionService>();
            var result = A.Fake<TransactionResponseModel>();
            var transactionobj = A.Fake<TransactionModel>();

            // set up a call to return a value
            A.CallTo(() => repo.Withdraw(transactionobj.Amount)).Returns(result);
        }

        [Fact]
        public void GetBalance_ReturnsResponseModel_WhenGetBalance()
        {

            var repo = A.Fake<ITransactionService>();
            var result = A.Fake<TransactionResponseModel>();
            var transactionobj = A.Fake<TransactionModel>();

            // set up a call to return a value
            A.CallTo(() => repo.GetBalance()).Returns(result);
        }

        [Fact]
        public void GetTransactions_ReturnsResponseModel_WhenGetTransactions()
        {

            var repo = A.Fake<ITransactionService>();
            var result = A.Fake<List<TransactionHistory>>();
            var transactionobj = A.Fake<TransactionModel>();

            // set up a call to return a value
            A.CallTo(() => repo.GetTransactions()).Returns(result);
        }
    }
}
