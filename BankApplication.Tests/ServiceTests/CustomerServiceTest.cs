using BankApplication.API.Controllers;
using BankApplication.DAL.Models.Entites;
using BankApplication.DAL.Models.VirtualModels;
using BankApplication.Service;
using FakeItEasy;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using Xunit;

namespace BankApplication.Tests.ServiceTests
{
    public class CustomerServiceTest
    {
        [Fact]
        public void LoginUser_ReturnsResponseModel_WhenLoginValidCustomer()
        {
            var loginobj = A.Fake<LoginModel>();
            var signupobj = A.Fake<Customer>();
            var profileobj = A.Fake<ProfileModel>();
            var repo = A.Fake<ICustomerService>();
            var result = A.Fake<ResponseModel>();

            // set up a call to return a value
            A.CallTo(() => repo.LoginUser(loginobj)).Returns(result);
        }

        [Fact]
        public void AddUser_ReturnsResponseModel_WhenSignUpValidCustomer()
        {
            var signupobj = A.Fake<Customer>();
            var repo = A.Fake<ICustomerService>();
            var result = A.Fake<ResponseModel>();

            // set up a call to return a value
            A.CallTo(() => repo.AddUser(signupobj)).Returns(result);
        }

        [Fact]
        public void GetCustomerProfileById_ReturnsProfileModel_WhenValidGuidCustomerId()
        {
            var profileobj = A.Fake<ProfileModel>();
            var repo = A.Fake<IProfileService>();
            var result = A.Fake<ResponseModel>();
            var signupobj = A.Fake<Customer>();

            // set up a call to return a value
            A.CallTo(() => repo.GetCustomerProfileById()).Returns(profileobj);
        }

        [Fact]
        public void EditUser_ReturnsProfileModel_WhenValidCustomer()
        {
            var repo = A.Fake<IProfileService>();
            var result = A.Fake<ResponseModel>();
            var signupobj = A.Fake<Customer>();

            // set up a call to return a value
            A.CallTo(() => repo.EditUser(signupobj)).Returns(result);
        }
    }
}
