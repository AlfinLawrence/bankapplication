﻿using System;
using System.Collections.Generic;
using System.Text;
using BankApplication.API.Controllers;
using BankApplication.DAL.Models.Entites;
using BankApplication.DAL.Models.VirtualModels;
using BankApplication.Service;
using FakeItEasy;
using Xunit;

namespace BankApplication.Tests.ControllerTests
{
    public class SignUpControllerTest
    {
        [Fact]
        public void SignUpUser_ReturnsResponseModel_WhenLoginValidCustomer()
        {
            //Arrange 
            var repo = A.Fake<ICustomerService>();
            var Customerobj = A.Fake<Customer>();
            var signupobj = A.Fake<Customer>();
            var result = A.Fake<ResponseModel>();
            A.CallTo(() => repo.AddUser(signupobj)).Returns(result);
            var controller = new SignUpController(repo);

            //Act
            var actionresult = controller.Post(signupobj);

            //Assert
            var response = actionresult as ResponseModel;
        }
    }
}
