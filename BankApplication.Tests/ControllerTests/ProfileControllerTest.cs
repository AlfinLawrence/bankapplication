﻿using BankApplication.API.Controllers;
using BankApplication.DAL.Models.Entites;
using BankApplication.DAL.Models.VirtualModels;
using BankApplication.Service;
using FakeItEasy;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace BankApplication.Tests.ControllerTests
{
    public class ProfileControllerTest
    {
        [Fact]
        public void GetProfile_ReturnsProfileModel_WhenLogin()
        {
            //Arrange 
            var repo = A.Fake<IProfileService>();
            var result = A.Fake<ProfileModel>();
            A.CallTo(() => repo.GetCustomerProfileById()).Returns(result);
            var controller = new ProfileController(repo);

            //Act
            var actionresult = controller.GetProfile();

            //Assert
            var response = actionresult as ProfileModel;
        }

        [Fact]
        public void EditProfile_ReturnsResponseModel_WhenLogin()
        {
            //Arrange 
            var repo = A.Fake<IProfileService>();
            var customerobj = A.Fake<Customer>();
            var result = A.Fake<ResponseModel>();
            A.CallTo(() => repo.EditUser(customerobj)).Returns(result);
            var controller = new ProfileController(repo);

            //Act
            var actionresult = controller.EditProfile(customerobj);

            //Assert
            var response = actionresult as ResponseModel;
        }
    }
}
