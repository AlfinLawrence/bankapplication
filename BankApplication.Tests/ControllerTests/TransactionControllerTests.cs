﻿using BankApplication.API.Controllers;
using BankApplication.DAL.Models.VirtualModels;
using BankApplication.Service;
using FakeItEasy;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace BankApplication.Tests.ControllerTests
{
    public class TransactionControllerTests
    {
        private readonly ITransactionService repo;

        public TransactionControllerTests()
        {
            repo = A.Fake<ITransactionService>();
        }

        [Fact]
        public void Deposit_ReturnsResponseModel_WhenDepositAmount()
        {
            //Arrange 
            var transactionobj = A.Fake<TransactionModel>();
            var result = A.Fake<TransactionResponseModel>();
            A.CallTo(() => repo.Deposit(transactionobj.Amount)).Returns(result);
            var controller = new TransactionController(repo);

            //Act
            var actionresult = controller.Deposit(transactionobj);

            //Assert
            var response = actionresult as TransactionResponseModel;
        }

        [Fact]
        public void WithDraw_ReturnsResponseModel_WhenDepositAmount()
        {
            //Arrange 
            var transactionobj = A.Fake<TransactionModel>();
            var result = A.Fake<TransactionResponseModel>();
            A.CallTo(() => repo.Deposit(transactionobj.Amount)).Returns(result);
            var controller = new TransactionController(repo);

            //Act
            var actionresult = controller.WithDraw(transactionobj);

            //Assert
            var response = actionresult as TransactionResponseModel;
        }

        [Fact]
        public void Balance_ReturnsResponseModel_WhenDepositAmount()
        {
            //Arrange 
            var result = A.Fake<TransactionResponseModel>();
            A.CallTo(() => repo.GetBalance()).Returns(result);
            var controller = new TransactionController(repo);

            //Act
            var actionresult = controller.Balance();

            //Assert
            var response = actionresult as TransactionResponseModel;
        }

        [Fact]
        public void GetTransactions_ReturnsResponseModel_WhenDepositAmount()
        {
            //Arrange 
            var result = A.Fake<List<TransactionHistory>>();
            A.CallTo(() => repo.GetTransactions()).Returns(result);
            var controller = new TransactionController(repo);

            //Act
            var actionresult = controller.GetTransactions();

            //Assert
            var response = actionresult as List<TransactionHistory>;
        }
    }
}
