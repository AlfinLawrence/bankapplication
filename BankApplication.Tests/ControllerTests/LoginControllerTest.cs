﻿using BankApplication.API.Controllers;
using BankApplication.DAL.Models.Entites;
using BankApplication.DAL.Models.VirtualModels;
using BankApplication.Service;
using FakeItEasy;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace BankApplication.Tests.ControllerTests
{
    public class LoginControllerTest
    {
        [Fact]
        public void LoginUser_ReturnsResponseModel_WhenLoginValidCustomer()
        {
            //Arrange 
            var repo = A.Fake<ICustomerService>();
            var loginobj = A.Fake<LoginModel>();
            var signupobj = A.Fake<Customer>();
            var result = A.Fake<ResponseModel>();
            A.CallTo(() => repo.LoginUser(loginobj)).Returns(result);
            var controller = new LoginController(repo);

            //Act
            var actionresult = controller.Post(loginobj);

            //Assert
            var response = actionresult as ResponseModel; 
        }
    }
}
